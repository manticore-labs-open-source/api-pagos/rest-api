# Código de Conducta
## Nuestro compromiso
Nuestro interés es fomentar un ambiente abierto y ameno para todo el mundo, nosotros como contribuyentes y mantenedores, nos comprometemos a hacer de la participación en nuestro proyecto y nuestra comunidad una experiencia libre de acoso para todos, independientemente de la edad, discapacidad, etnia, identidad y expresión de género, nivel de experiencia, educación, estado socioeconómico, nacionalidad, apariencia personal, raza, religión o identidad y orientación sexual.

## Nuestros estándares
Algunos ejemplos de comportamiento que contribuyen a crear un ambiente positivo pueden incluir:
- Uso de lenguaje cordial e inclusivo.
- Ser respetuoso de los diferentes puntos de vista y experiencias.
- Aceptar las críticas constructivas.
- Enfocarse en lo que es mejor para la comunidad.

Algunos ejemplos de comportamiento inaceptable por parte de los participantes pueden ser:
- Uso de lenguage o imágenes sexualizadas.
- Comentatios insultantes y/o despectivos y ataques personales o políticos.
- Acoso público o privado.
- Publicar información privada de otros integrantes, como la dirección personal sin autorización explícita.
- Otra conducta la cual puede ser razonablemente considerada inapropiada en un entorno profesional.

## Nuestra responsablidad
Los mantenedores del proyecto son responsables de clarificar los estándares de comportamiento aceptable y se espera que tomen acciones justas y apropiadas en respuesta a ciertos comportamientos inapropiados.

Los mantenedores del proyecto tienen el derecho y responsibilidad de remover, editar, o rechazar comentarios, commits, código, ediciones de wiki, problemas y otras contribuciones que no van acorde a este código de conducta. También están autorizados para bloquear temporal o permanentemente la participación de cualquier colaborador que tenga un comportamiento aparentemente inapropiado, amenazante, ofensivo o dañino.

## Alcance
Este Código de conducta aplica a todo el proyecto, también aplica cuando cualquier persona está representando el proyecto o a la comunidad en espacios públicos. Algunos ejemplos de representación del proyecto o su comunidad pueden ser el uso de un e-mail oficial del proyecto, publicaciones en cuentas oficiales, o si se participa como representante designado en un evento online u offline. La representación del proyecto deberá ser clarificada y definida por parte de los mantenedores.

## Aplicación
Situaciones de abuso, acoso u otro tipo de comportamiento inapropiado deberá ser reportado a través del correo : unipaygw@gmail.com. Todas las quejas serán revisadas e investigadas y su resultado deberá ser una respuesta acorde a las circunstancias. El equipo del proyecto está obligado a mantener confidencialidad con el reportador del incidente. 

Los mantenedores del proyecto quienes no sigan las reglas o apliquen el código de conducta en buena fe, podrán afrontar repercusiones temporales o permanentes como determinen los otros miembros líderes del proyecto. EL cumplimiento de este código también incluye a los autores o líderes del proyecto.

## Atribución
Este Código de conducta está adaptado del [Convenio de Conducta](https://www.contributor-covenant.org/version/1/4/code-of-conduct/)
