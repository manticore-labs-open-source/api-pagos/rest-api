# REST API - Plataforma Open Source para el fácil uso de botones de pago  (UniPayGW)


## Introducción

UniPayGW es una plataforma de código abierto hecha para poder integrar múltiples pasarelas de pago (actualmente cuenta con soporte para dos) a un sitio de comercio eléctrónico. La plataforma está programada en TypeScript y diseñada para ser usada con sistemas de base de datos NoSQL, pero también es compatible con cualquier sistema SQL gracias a TypeORM.


## ¿Por qué usarla?

En la actualidad existen varias pasarelas de pago alrededor del mundo que nos ofrecen diferentes beneficios y precios de comisión al momento de procesar las transacciones de un comercio electrónico. Esta API permite la fácil integración de una o más pasarelas de pagos en un sitio de comercio electrónico, sin necesidad de estudiar o revisar la documentación de cada pasarela de pago, sino únicamente la perteneciente a este proyecto. 

Actualmente, UniPayGW puede soportar las siguientes pasarelas:


| Nombre | País    |
|--------|---------|
| Kushki | Ecuador |
| Stripe | USA     |  

Esperamos que en un futuro se pueda dar soporte a más pasarelas.

## Prerrequisitos

- Instalación de `npm` para el manejo de paquetes de node.
- Instalación de `git` para poder clonar el proyecto de manera local.
- Instalación de [GitHub Desktop](https://itnext.io/how-to-use-github-desktop-with-gitlab-cd4d2de3d104) o [GitKraken](https://support.gitkraken.com/integrations/gitlab/) si se quiere hacer uso de un cliente visual.

## Instalación

Existen varias formas para poder instalar MultiPayGateways.

 ### Descarga
  Se puede descargar el proyecto usando los links:

 | Método    |  Instrucciones                                                                                                                             |
 |-----------|--------------------------------------------------------------------------------------------------------------------------------------------|
 | .zip      | Descargar el proyecto de https://gitlab.com/manticore-labs-open-source/api-pagos/rest-api/-/archive/master/rest-api-master.zip,            |
 |           | a continuación instalar las dependencias mediante `npm i`                                                                                  |
 | .tar.gz   | Descargar el proyecto de https://gitlab.com/manticore-labs-open-source/api-pagos/rest-api/-/archive/master/rest-api-master.tar.gz,         |
 |           | a continuación instalar las dependencias mediante `npm i`                                                                                  |
 | .tar.bz2  | Descargar el proyecto de https://gitlab.com/manticore-labs-open-source/api-pagos/rest-api/-/archive/master/rest-api-master.tar.bz2,        |
 |           | a continuación instalar las dependencias mediante `npm i`                                                                                  |
 | .tar      | Descargar el proyecto de https://gitlab.com/manticore-labs-open-source/api-pagos/rest-api/-/archive/master/rest-api-master.tar,            |
 |           | a continuación instalar las dependencias mediante `npm i`                                                                                  |
 | git       | Para clonar mediante HTTPS utilizar el siguiente comando: git clone https://gitlab.com/manticore-labs-open-source/api-pagos/rest-api.git   |
 |           | Si se necesita instalar mediante SSH utilizar el siguiente comando: git@gitlab.com:manticore-labs-open-source/api-pagos/rest-api.git     |

## Uso del API
Una vez decargado el proyecto e instaladas todas las dependencias, sed eberá ejecutar el comando `npm run start` para levantar el servidor.\
Para poder utilizar el API, será necesario leer la [documentación](https://app.swaggerhub.com/apis/UniPayGW5/UniPay/1.0.0) 

## ¿Cómo puedo contribuir a UniPayGW?
Antes de poder participar en nuestra comunidad, por favor lee nuestro [código de conducta](https://gitlab.com/manticore-labs-open-source/api-pagos/rest-api/-/blob/master/UniPayGW/CODIGO_DE_CONDUCTA.md).

Puedes aportar como revisor de merge request o hacer un merge request de un problema abierto.\
para más información, visita el siguiente enlace, [¿Cómo contribuir a UniPayGW?](https://gitlab.com/manticore-labs-open-source/api-pagos/rest-api/-/wikis/%C2%BFC%C3%B3mo-contribuir-a-UniPayGW%3F)

## Guía de compilación

Para la compilación del proyecto se han definido los siguientes pasos:

1. Ejecutar el comando `npm i` para instalar todas las dependencias utilizadas para el proyecto.
2. Instalar el paquete de node mustache con el comando `npm i -g mustache`.
3. Se debe crear un archivo var.json con la siguiente estructura:
    ```js
    {
   "AUTH_DOMAIN":"dominio de auth0 necesario para la autenticación",
   "AUTH_CLIENT_ID":"identificador de auth0",
   "AUTH_CLIENT_SECRET":"cualquier código necesario para la autenticación.",
   "AUTH_CALLBACK_URL":"http://dev.api-tesis.site/callback",
   "MONGO_PASSWORD":"contraseña de la base de datos de mongoDb",
   "STRIPE_URL":"https://api.stripe.com/v1/",
   "KUSHKI_URL":"https://api-uat.kushkipagos.com/card/v1/",
   "PORT":"puerto en el que correrá el API",
   "SECRET_KEY":"puede ser el mismo que `AUTH_CLIENT_SECRET`",
   "SPA_PATH":"url de archivos estáticos para el aplicativo de estado de transacciones y pasarelas",
   "SPA_EXAMPLE_PATH":"url de archivos estáticos para el aplicativo de creación de transacciones",
   "MONGO_DB":"base de datos de mongoDb"
    }
    ```
4. Se ejecuta el comando `cat var.json | mustache - ./src/environment/environment.mustache > ./src/environment/environment.ts` que creará el archivo de `environment.ts` a partir del archivo `environmetn.mustache` y los valores de las variables de ambiente incluidos en `var.json`.
5. Se ejecuta el comando `cat ./src/environment/environment.ts` para asegurarnos que se generó el archivo de manera correcta.
6. Se ejecuta el comando para construir el proyecto `npm run build`.
7. Es necesario verificar que se generar los archivos correspondientes mediante el comando `ls`.
8. La carpeta `./dist` deberá haberse creado de manera correcta por lo que se debe verificar con el comando `ls ./dist`.
9. Se ejecuta el siguiente comando en la carpeta donde se almacenará el proyecto `cd <carpeta donde se almacenará el código fuente> && sudo rm -rf * && mkdir dist && mkdir views && mkdir public && exit`  para que se eliminen las carpetas en caso de existir.
10. A continuación se deberá copiar el archivo `package.json` con el comando `./package.json <carpeta donde se almacenará el código fuente>/package.json`
11. Igual que en el paso anterior se ejecutaran los siguientes comandos para copiar el código fuente en la carpeta contenedora `./package-lock.json <carpeta donde se almacenará el código fuente>/package-lock.json`, `./dist/* <carpeta donde se almacenará el código fuente>/dist`, `./views/* <carpeta donde se almacenará el código fuente>/views`, `./public/* <carpeta donde se almacenará el código fuente>/public`.
12. Finalmente se ejecuta el siguiente comandop para instalar las dependencias necesarias e iniciar el servidor. `cd <carpeta donde se almacenará el código fuente> && ls && pm2 stop api-rest-dev && npm i && pm2 start api-rest-dev && exit`

**NOTA:** Como el proyecto hasta el momento está diseñado para soportar dos pasarelas de pago (Kushki y Stripe), solo existen dos variables de entorno relacionadas a las pasarelas de pago, en este caso es KUSHKI_URL y STRIPE_URL. Si se incluiría otra pasarela de pago en el API REST, se debería agregar las variables de entorno correspondientes en el formato 'nombreDePasarela_URL'  


## Licencia

 UniPayGW está bajo la licencia [MIT license](https://gitlab.com/manticore-labs-open-source/api-pagos/rest-api/-/blob/master/LICENSE)












