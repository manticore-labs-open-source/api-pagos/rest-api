import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { GatewaysModule } from './gateways/gateways.module';
import { CardTransactionsModule } from './cardTransactions/cardTransactions.module';
import { ClientModule } from './client/client.module';
import { ApiAuthMiddleware, AppMiddleware } from './app.middleware';
import { Auth0Strategy } from './auth0.strategy';
import { authenticate } from 'passport';
import { environment } from './environment/environment';
import { ClientMiddleware } from './client/client.middleware';
import { CardTransactionsController } from './cardTransactions/cardTransactions.controller';
import { GatewaysController } from './gateways/gateways.controller';
import { ExampleController } from './example/example.controller';
import { ExampleModule } from './example/example.module';
import { ExampleMiddleware } from './example/example.middleware';

@Module({
  imports: [
    MongooseModule.forRoot(
      `mongodb+srv://gateways:${environment.mongoDBPassword}@restapi.59ofh.mongodb.net/${environment.mongoDB}?retryWrites=true&w=majority`,
    ),
    GatewaysModule,
    CardTransactionsModule,
    ClientModule,
    ExampleModule
  ],
  controllers: [AppController],
  providers: [Auth0Strategy],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ClientMiddleware)
      .forRoutes({ path: '/', method: RequestMethod.GET })
      .apply(
        authenticate('auth0', {
          clientID: environment.auth0ClientId,
          domain: environment.auth0Domain,
          redirectUri: environment.auth0CallbackUrl,
          audience: 'https://' + environment.auth0Domain + '/userinfo',
          responseType: 'code',
          scope: 'openid profile',
        }),
      )
      .forRoutes({ path: '/login', method: RequestMethod.ALL })
      .apply(
        authenticate('auth0', {
          successRedirect: '/admin/transactions',
          failureRedirect: '/',
        }),
      )
      .forRoutes({ path: '/callback', method: RequestMethod.ALL })
      .apply(AppMiddleware)
      .forRoutes({ path: '/user', method: RequestMethod.ALL })
      .apply(ApiAuthMiddleware)
      .forRoutes(CardTransactionsController)
      .apply(ApiAuthMiddleware)
      .forRoutes(GatewaysController)
      .apply(AppMiddleware)
      .forRoutes(ExampleController)
      .apply(ExampleMiddleware)
      .forRoutes(ExampleController);
  }
}
