import { HttpException, Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class AppMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    if ((!req.isAuthenticated || !req.isAuthenticated()) && !req.path.includes('example')) {
      if (req.session) {
        req.session.returnTo = req.originalUrl || req.url;
      }
      return res.redirect('/');
    }
    next();
  }
}

@Injectable()
export class ApiAuthMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    const valid_routes = ['card/token', 'card/charges'];

    if (
      (!req.isAuthenticated || !req.isAuthenticated()) &&
      valid_routes.includes(req.path)
    ) {
      throw new HttpException('Unauthorized', 401);
    }
    next();
  }
}
