import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import { environment } from '../environment/environment';
import * as path from 'path';

@Injectable()
export class ExampleService {
  public async getApp(): Promise<string> {
    const filePath = path.resolve(path.join(environment.spaExamplePath, 'index.html'));

    return new Promise((resolve, reject) => {
      fs.readFile(filePath, 'utf8', (err: NodeJS.ErrnoException, data: string) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }
}
