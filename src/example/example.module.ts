import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { ExampleController } from './example.controller';
import { ExampleService } from './example.service';
import { ExampleMiddleware } from './example.middleware';
import { AppMiddleware } from '../app.middleware';

@Module({
  controllers: [ExampleController],
  providers: [ExampleService]
})
export class ExampleModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ExampleMiddleware)
      .forRoutes(ExampleController)
      .apply(AppMiddleware)
      .forRoutes(ExampleController);
  }
}
