import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GatewaySchema } from './gateways.model';
import { GatewaysService } from './gateways.service';
import { GatewaysController } from './gateways.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Gateway', schema: GatewaySchema }]),
  ],
  controllers: [GatewaysController],
  providers: [GatewaysService],
  exports: [GatewaysService]
})
export class GatewaysModule {}
