import { Document, Schema } from 'mongoose';
import { IsEnum, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

const GatewayCredentialSchema = new Schema({
  publicCredential: { type: String, required: true },
  privateCredential: { type: String, required: true },
});

export const GatewaySchema = new Schema({
  created: { type: Number, required: true },
  name: { type: String, required: true },
  credential: { type: GatewayCredentialSchema, required: true },
  type: { type: String, required: true },
  status: { type: String, required: true },
  provider: { type: String, required: true },
});

export interface Gateway extends Document {
  id: string;
  created: number;
  name: string;
  credential: GatewayCredential;
  type: string;
  status: string;
  provider: string;
}

export interface GatewayCredential {
  publicCredential: string;
  privateCredential: string;
}

export enum GatewayType {
  production = 'production',
  sandbox = 'sandbox',
}

export enum GatewayProvider {
  kushki = 'kushki',
  stripe = 'stripe',
}

class CredentialDto {
  @IsString()
  @IsNotEmpty()
  publicCredential: string;

  @IsString()
  @IsNotEmpty()
  privateCredential: string;
}

export class CreateGatewayDto {
  @IsEnum(GatewayType)
  type: GatewayType;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsEnum(GatewayProvider)
  provider: GatewayProvider;

  @ValidateNested()
  @Type(() => CredentialDto)
  credential: CredentialDto;
}

export enum GatewayStatus {
  active = 'active',
  inactive = 'inactive',
}

export class UpdateGatewayDto {
  @IsEnum(GatewayStatus)
  status: GatewayStatus;

  @IsString()
  @IsNotEmpty()
  name: string;

  @ValidateNested()
  @Type(() => CredentialDto)
  credential: CredentialDto;
}
