import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  Gateway,
  GatewayCredential, GatewayProvider,
  GatewayStatus,
  GatewayType,
} from './gateways.model';

@Injectable()
export class GatewaysService {
  constructor(
    @InjectModel('Gateway') private readonly _gatewayModel: Model<Gateway>,
  ) {}

  async insertGateway(
    name: string,
    type: GatewayType,
    credential: GatewayCredential,
    provider: GatewayProvider
  ): Promise<string> {
    const newGateway = new this._gatewayModel({
      name,
      type,
      credential,
      provider,
      created: new Date().getTime(),
      status: 'active',
    });

    const result = await newGateway.save();

    return result.id;
  }

  async getGateways(): Promise<Record<string, any>[]> {
    const gateways = await this._gatewayModel.find().exec();

    return gateways.map((gateway: Gateway) => ({
      id: gateway.id,
      created: gateway.created,
      name: gateway.name,
      credential: {
        publicCredential: gateway.credential.publicCredential,
        privateCredential: gateway.credential.privateCredential,
      },
      type: gateway.type,
      status: gateway.status,
      provider: gateway.provider,
    }));
  }

  async getGatewayById(id: string): Promise<Record<string, any>> {
    const gateway = await this._findGateway(id);

    if (gateway.status !== GatewayStatus.active) {
      throw new BadRequestException('Gateway not active.');
    }

    return {
      id: gateway.id,
      created: gateway.created,
      name: gateway.name,
      credential: {
        publicCredential: gateway.credential.publicCredential,
        privateCredential: gateway.credential.privateCredential,
      },
      type: gateway.type,
      status: gateway.status,
      provider: gateway.provider,
    };
  }

  async updateGateway(
    id: string,
    gatewayFields: {
      name?: string;
      status?: GatewayStatus;
      credential?: GatewayCredential;
    },
  ): Promise<void> {
    const gateway = await this._findGateway(id);

    if (gatewayFields.name) {
      gateway.name = gatewayFields.name;
    }
    if (gatewayFields.status) {
      gateway.status = gatewayFields.status;
    }
    if (gatewayFields.credential) {
      gateway.credential.publicCredential =
        gatewayFields.credential.publicCredential;
      gateway.credential.privateCredential =
        gatewayFields.credential.privateCredential;
    }

    await gateway.save();
  }

  async deleteGateway(id: string): Promise<void> {
    const result = await this._gatewayModel.deleteOne({ _id: id }).exec();

    if (result.n === 0) {
      throw new NotFoundException('Gateway not found.');
    }
  }

  private async _findGateway(id: string): Promise<Gateway> {
    let gateway;

    try {
      gateway = await this._gatewayModel.findById(id).exec();
    } catch (error) {
      throw new NotFoundException('Gateway not found.');
    }

    if (!gateway) {
      throw new NotFoundException('Gateway not found.');
    }

    return gateway;
  }
}
