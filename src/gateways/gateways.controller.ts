import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { GatewaysService } from './gateways.service';
import { CreateGatewayDto, UpdateGatewayDto } from './gateways.model';

@Controller('api/gateways')
export class GatewaysController {
  constructor(private readonly _gatewaysService: GatewaysService) {}

  @Post()
  async addGateway(
    @Body() createGatewayDto: CreateGatewayDto,
  ): Promise<{ id: string }> {
    const id = await this._gatewaysService.insertGateway(
      createGatewayDto.name,
      createGatewayDto.type,
      createGatewayDto.credential,
      createGatewayDto.provider,
    );

    return { id };
  }

  @Get()
  async getAllGateways(): Promise<Record<string, any>[]> {
    return await this._gatewaysService.getGateways();
  }

  @Get('providers')
  getProviders(): { value: string; name: string }[] {
    return [
      { value: 'kushki', name: 'Kushki' },
      { value: 'stripe', name: 'Stripe' },
    ];
  }

  @Patch(':id')
  async updateGateway(
    @Param('id') id: string,
    @Body() updateGatewayDto: UpdateGatewayDto,
  ): Promise<null> {
    await this._gatewaysService.updateGateway(id, {
      name: updateGatewayDto.name,
      status: updateGatewayDto.status,
      credential: updateGatewayDto.credential,
    });

    return null;
  }

  @Delete(':id')
  async removeGateway(@Param('id') id: string): Promise<null> {
    await this._gatewaysService.deleteGateway(id);

    return null;
  }
}
