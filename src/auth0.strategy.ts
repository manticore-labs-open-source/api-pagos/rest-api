import { use, serializeUser, deserializeUser } from 'passport';
import { Strategy } from 'passport-auth0';
import { Injectable } from '@nestjs/common';
import { environment } from './environment/environment';

@Injectable()
export class Auth0Strategy extends Strategy {
  constructor() {
    super(
      {
        domain: environment.auth0Domain,
        clientID: environment.auth0ClientId,
        clientSecret: environment.auth0ClientSecret,
        callbackURL: environment.auth0CallbackUrl,
      },
      async (accessToken, refreshToken, extraParams, profile, done) => {
        return done(null, profile);
      },
    );
    use(this);

    serializeUser((user, done) => {
      done(null, user);
    });

    deserializeUser((user, done) => {
      done(null, user);
    });
  }
}
