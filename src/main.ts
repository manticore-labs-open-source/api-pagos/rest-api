import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';
import * as session from 'express-session';
import * as passport from 'passport';
import { NestExpressApplication } from '@nestjs/platform-express';
import { environment } from './environment/environment';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.set('view engine', 'pug');
  app.use(express.static("public"));
  app.use(session({
    secret: environment.secretKey,
    resave: true,
    saveUninitialized: true
  }));
  app.use(passport.initialize());
  app.use(passport.session());
  app.useGlobalPipes(new ValidationPipe());

  const PORT = environment.port;

  await app.listen(PORT, () => {
    console.log(`Nest app is listening on port ${PORT}.`);
  });
}

bootstrap();
