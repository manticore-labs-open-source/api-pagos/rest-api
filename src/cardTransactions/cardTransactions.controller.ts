import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { CardTransactionsService } from './cardTransactions.service';
import {
  Amount,
  Card,
  CardTransaction,
  ChargeDto,
  TokenDto,
} from './cardTransactions.model';
import { Observable } from 'rxjs';

@Controller('api/card')
export class CardTransactionsController {
  constructor(
    private readonly _cardTransactionService: CardTransactionsService,
  ) {}

  @Post('token')
  token(@Body() tokenDto: TokenDto): Observable<{ token: string }> {
    return this._cardTransactionService.token(
      tokenDto.card,
      tokenDto.gatewayId,
      tokenDto.amount,
      tokenDto.currency,
    );
  }

  @Post('charge')
  charge(@Body() chargeDto: ChargeDto): Observable<Record<string, any>> {
    return this._cardTransactionService.charge(
      chargeDto.token,
      chargeDto.amount,
      chargeDto.currency,
      chargeDto.description,
      chargeDto.metadata,
    );
  }

  @Get('transaction/:id')
  getTransactionById(@Param('id') id: string): Observable<Record<string, any>> {
    return this._cardTransactionService.getTransactionById(id);
  }

  @Get('transaction')
  getTransactions(
    @Query('page') page,
    @Query('lowerDate') lowerDate,
    @Query('upperDate') upperDate,
  ): Observable<Record<string, any>[]> {
    return this._cardTransactionService.getTransactions(
      Number(page),
      Number(lowerDate),
      Number(upperDate),
    );
  }
}
