import { Injectable, NotFoundException } from '@nestjs/common';
import { Amount, Card, CardTransaction } from './cardTransactions.model';
import {
  StripeChargeResponse,
  StripeService,
  StripeTokenResponse,
} from './providers/stripe.service';
import { Gateway } from '../gateways/gateways.model';
import { forkJoin, from, Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GatewaysService } from '../gateways/gateways.service';
import { get } from 'lodash';
import { KushkiService } from './providers/kushki.service';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as cleaner from 'deep-cleaner';

@Injectable()
export class CardTransactionsService {
  constructor(
    @InjectModel('CardTransaction')
    private readonly _cardTransactionModel: Model<CardTransaction>,
    private readonly _stripeService: StripeService,
    private readonly _kushkiService: KushkiService,
    private readonly _gatewaysService: GatewaysService,
  ) {}

  token(
    card: Card,
    gatewayId: string,
    amount: number,
    currency: string,
  ): Observable<{ token: string }> {
    return of(1).pipe(
      mergeMap(() => from(this._gatewaysService.getGatewayById(gatewayId))),
      mergeMap((gateway: Gateway) => {
        let token_observable: Observable<StripeTokenResponse>;

        switch (gateway.provider) {
          case 'kushki':
            token_observable = this._kushkiService.token(
              card,
              gateway,
              amount,
              currency,
            );
            break;
          case 'stripe':
          default:
            token_observable = this._stripeService.token(card, gateway);
        }

        return token_observable;
      }),
      mergeMap((response: StripeTokenResponse) => {
        const newCardTransaction = new this._cardTransactionModel({
          gatewayId,
          token: {
            token: response.id,
            created: response.created,
            card: {
              bin: card.number.slice(0, 6),
              brand: response.card.brand,
              country: response.card.country,
              type: response.card.funding,
              cardId: response.card.id,
              last4: response.card.last4,
              name: card.name,
            },
          },
        });

        return from(newCardTransaction.save());
      }),
      map((result: CardTransaction) => ({ token: result.token.token })),
    );
  }

  charge(
    token: string,
    amount: Amount,
    currency: string,
    description?: string,
    metadata?: Record<string, any>,
  ): Observable<Record<string, any>> {
    return of(1).pipe(
      mergeMap(() => from(this._findTransactionByToken(token))),
      mergeMap((transaction: CardTransaction) =>
        forkJoin([
          of(transaction),
          from(this._gatewaysService.getGatewayById(transaction.gatewayId)),
        ]),
      ),
      mergeMap(([transaction, gateway]: [CardTransaction, Gateway]) => {
        let charge_observable: Observable<StripeChargeResponse>;

        switch (gateway.provider) {
          case 'kushki':
            charge_observable = this._kushkiService.charge(
              gateway,
              amount,
              transaction.token.token,
              currency,
              description,
              metadata,
            );
            break;
          case 'stripe':
          default:
            charge_observable = this._stripeService.charge(
              gateway,
              amount,
              transaction.token.token,
              currency,
              description,
              metadata,
            );
        }

        return forkJoin([of(transaction), charge_observable]);
      }),
      mergeMap(
        ([transaction, response]: [CardTransaction, StripeChargeResponse]) => {
          let new_trx = transaction;

          new_trx.metadata = response.metadata;
          new_trx.amount = response.amount;
          new_trx.created = response.created;
          new_trx.currency = currency;
          new_trx.details = get(response, 'details');
          new_trx.description = response.description;
          new_trx.responseCode = get(response, 'responseCode');
          new_trx.responseText = get(response, 'responseText');
          new_trx.status = response.status;
          new_trx.transactionReference = response.id;
          new_trx.type = 'SALE';
          new_trx.provider = get(response, 'provider');

          if (Boolean(transaction.transactionReference))
            new_trx = new this._cardTransactionModel({
              gatewayId: new_trx.gatewayId,
              token: {
                token: get(new_trx, 'token.token'),
                created: get(new_trx, 'token.created'),
                card: {
                  bin: get(new_trx, 'token.card.bin'),
                  brand: get(new_trx, 'token.card.brand'),
                  country: get(new_trx, 'token.card.country'),
                  type: get(new_trx, 'token.card.type'),
                  cardId: get(new_trx, 'token.card.cardId'),
                  last4: get(new_trx, 'token.card.last4'),
                  name: get(new_trx, 'token.card.name'),
                },
              },
              amount: new_trx.amount,
              created: new_trx.created,
              currency: new_trx.currency,
              description: new_trx.description,
              details: {
                ...new_trx.details,
              },
              metadata: {
                ...new_trx.metadata,
              },
              provider: new_trx.provider,
              responseCode: new_trx.responseCode,
              responseText: new_trx.responseText,
              status: new_trx.status,
              transactionReference: new_trx.transactionReference,
              type: new_trx.type,
            });

          return from(new_trx.save());
        },
      ),
      map((result: CardTransaction) => ({
        transactionId: result.id,
        responseCode: result.responseCode,
        responseText: result.responseText,
        status: result.status,
      })),
    );
  }

  getTransactionById(transactionId: string): Observable<Record<string, any>> {
    return of(1).pipe(
      mergeMap(() => from(this._findTransationById(transactionId))),
      map((trx: CardTransaction) => {
        if (!Boolean(trx.transactionReference))
          throw new NotFoundException('Transaction not found.');

        const transaction: Record<string, any> = {
          id: trx.id,
          gatewayId: trx.gatewayId,
          token: {
            id: get(trx, 'token.token'),
            created: get(trx, 'token.created'),
            card: {
              bin: get(trx, 'token.card.bin'),
              brand: get(trx, 'token.card.brand'),
              country: get(trx, 'token.card.country'),
              type: get(trx, 'token.card.type'),
              cardId: get(trx, 'token.card.cardId'),
              last4: get(trx, 'token.card.last4'),
              name: get(trx, 'token.card.name'),
            },
          },
          amount: trx.amount,
          created: trx.created,
          currency: trx.currency,
          description: trx.description,
          details: {
            ...trx.details,
          },
          metadata: {
            ...trx.metadata,
          },
          provider: trx.provider,
          responseCode: trx.responseCode,
          responseText: trx.responseText,
          status: trx.status,
          transactionReference: trx.transactionReference,
          type: trx.type,
        };

        return cleaner(transaction);
      }),
    );
  }

  getTransactions(
    page: number,
    lowerDate?: number,
    upperDate?: number,
  ): Observable<Record<string, any>[]> {
    return of(1).pipe(
      mergeMap(() =>
        this._getPaginatedTransactions(page, lowerDate, upperDate),
      ),
      map((trxs: CardTransaction[]) =>
        trxs
          .filter((trx: CardTransaction) => Boolean(trx.transactionReference))
          .map((trx: CardTransaction) =>
            cleaner({
              id: trx.id,
              gatewayId: trx.gatewayId,
              token: {
                id: get(trx, 'token.token'),
                created: get(trx, 'token.created'),
                card: {
                  bin: get(trx, 'token.card.bin'),
                  brand: get(trx, 'token.card.brand'),
                  country: get(trx, 'token.card.country'),
                  type: get(trx, 'token.card.type'),
                  cardId: get(trx, 'token.card.cardId'),
                  last4: get(trx, 'token.card.last4'),
                  name: get(trx, 'token.card.name'),
                },
              },
              amount: trx.amount,
              created: trx.created,
              currency: trx.currency,
              description: trx.description,
              details: {
                ...trx.details,
              },
              metadata: {
                ...trx.metadata,
              },
              provider: trx.provider,
              responseCode: trx.responseCode,
              responseText: trx.responseText,
              status: trx.status,
              transactionReference: trx.transactionReference,
              type: trx.type,
            }),
          ),
      ),
    );
  }

  private _getPaginatedTransactions(
    page: number,
    lowerDate?: number,
    upperDate?: number,
  ): Observable<CardTransaction[]> {
    return of(1).pipe(
      mergeMap(() => {
        const limit = 10;
        const skips = limit * (page - 1);

        if (!isNaN(lowerDate) && !isNaN(upperDate))
          return from(
            this._cardTransactionModel
              .find({ created: { $gte: lowerDate, $lt: upperDate } })
              .skip(skips)
              .limit(limit)
              .exec(),
          );

        return from(
          this._cardTransactionModel
            .find()
            .skip(skips)
            .limit(limit)
            .exec(),
        );
      }),
    );
  }

  private async _findTransactionByToken(
    token: string,
  ): Promise<CardTransaction> {
    let cardTransaction;

    try {
      cardTransaction = await this._cardTransactionModel
        .findOne({ 'token.token': token })
        .exec();
    } catch (error) {
      throw new NotFoundException('Transaction not found.');
    }

    if (!cardTransaction) {
      throw new NotFoundException('Transaction not found.');
    }

    return cardTransaction;
  }

  private async _findTransationById(id: string): Promise<CardTransaction> {
    let gateway;

    try {
      gateway = await this._cardTransactionModel.findById(id).exec();
    } catch (error) {
      throw new NotFoundException('Transaction not found.');
    }

    if (!gateway) {
      throw new NotFoundException('Transaction not found.');
    }

    return gateway;
  }
}
