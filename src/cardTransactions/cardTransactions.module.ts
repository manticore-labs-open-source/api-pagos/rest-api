import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CardTransactionsService } from './cardTransactions.service';
import { CardTransactionsController } from './cardTransactions.controller';
import { GatewaysModule } from '../gateways/gateways.module';
import { StripeService } from './providers/stripe.service';
import { CardTransactionSchema } from './cardTransactions.model';
import { KushkiService } from './providers/kushki.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'CardTransaction', schema: CardTransactionSchema },
    ]),
    GatewaysModule,
    HttpModule,
  ],
  controllers: [CardTransactionsController],
  providers: [CardTransactionsService, StripeService, KushkiService],
})
export class CardTransactionsModule {}
