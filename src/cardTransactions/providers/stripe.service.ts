import { HttpException, HttpService, Injectable } from '@nestjs/common';
import { Gateway } from '../../gateways/gateways.model';
import { Amount, Card } from '../cardTransactions.model';
import { Observable, of } from 'rxjs';
import { AxiosError, AxiosResponse } from 'axios';
import { stringify } from 'querystring';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environment/environment';
import { get, set } from 'lodash';
import { v4 } from 'uuid';

export interface StripeTokenResponse {
  id: string;
  object: string;
  card: {
    id: string;
    object: string;
    brand: string;
    country: string;
    cvc_check: string;
    exp_month: number;
    exp_year: number;
    funding: string;
    last4: string;
    metadata: Record<string, any>;
    name: string;
  };
  client_ip: string;
  created: number;
  type: string;
  used: boolean;
}

export interface StripeChargeResponse {
  id: string;
  object: string;
  amount: number;
  amount_refunded: number;
  balance_transaction: string;
  billing_details: Record<string, any>;
  captured: boolean;
  description: string;
  disputed: boolean;
  failure_code: string;
  failure_message: string;
  metadata: Record<string, any>;
  paid: boolean;
  payment_method: string;
  payment_method_details: Record<string, any>;
  receipt_url: string;
  refunded: boolean;
  refunds: Record<string, any>;
  status: string;
  source: string;
  created: number;
}

@Injectable()
export class StripeService {
  constructor(private readonly _httpService: HttpService) {}

  token(card: Card, gateway: Gateway): Observable<StripeTokenResponse> {
    return this._httpService
      .post<StripeTokenResponse>(
        `${environment.stripeBaseURL}tokens`,
        stringify({
          'card[number]': card.number,
          'card[name]': card.name,
          'card[exp_month]': card.expiryMonth,
          'card[exp_year]': card.expiryYear,
          'card[cvc]': card.cvc,
        }),
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          auth: {
            username: gateway.credential.publicCredential,
            password: '',
          },
        },
      )
      .pipe(
        map((response: AxiosResponse<StripeTokenResponse>) => response.data),
        catchError((error: AxiosError) => {
          throw new HttpException(
            error.response.data.error.message,
            error.response.status,
          );
        }),
      );
  }

  charge(
    gateway: Gateway,
    amount: Amount,
    token: string,
    currency: string,
    description?: string,
    metadata?: Record<string, any>,
  ): Observable<StripeChargeResponse> {
    let metadata_obj;

    if (metadata) {
      metadata_obj = {};

      Object.keys(metadata).forEach(
        (value: string) =>
          (metadata_obj[`metadata[${value}]`] = metadata[value]),
      );
    }

    return this._httpService
      .post<StripeChargeResponse>(
        `${environment.stripeBaseURL}charges`,
        stringify({
          currency,
          description,
          ...metadata_obj,
          amount: (Number(amount.subtotal) + Number(amount.subtotalTax)) * 100,
          source: token,
        }),
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          auth: {
            username: gateway.credential.privateCredential,
            password: '',
          },
        },
      )
      .pipe(
        map((response: AxiosResponse<StripeChargeResponse>) => {
          set(response, 'data.details', { ...response.data });
          set(response, 'data.amount', response.data.amount / 100);
          set(response, 'data.currency', currency);
          set(response, 'data.responseCode', '000');
          set(response, 'data.responseText', 'Approved transaction');
          set(response, 'data.created', response.data.created * 1000);
          set(response, 'data.provider', 'stripe');

          return response.data;
        }),
        catchError((error: AxiosError) => {
          if (error.response.status !== 402)
            throw new HttpException(
              error.response.data.error.message,
              error.response.status,
            );

          const response = {
            currency,
            id: v4(),
            created: new Date().getTime(),
            amount: Number(amount.subtotal) + Number(amount.subtotalTax),
            status: 'declined',
            provider: 'stripe'
          };

          set(response, 'details', error.response.data);
          set(
            response,
            'responseCode',
            get(error, 'response.data.error.decline_code'),
          );
          set(
            response,
            'responseText',
            get(error, 'response.data.error.message'),
          );
          set(response, 'metadata', { ...metadata });
          set(response, 'description', description);

          return of(<StripeChargeResponse>(<unknown>response));
        }),
      );
  }
}
