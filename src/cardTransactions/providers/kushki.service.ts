import { HttpException, HttpService, Injectable } from '@nestjs/common';
import { Gateway } from '../../gateways/gateways.model';
import { Amount, Card } from '../cardTransactions.model';
import { forkJoin, Observable, of } from 'rxjs';
import { AxiosError, AxiosResponse } from 'axios';
import { stringify } from 'querystring';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { environment } from '../../environment/environment';
import { get, set } from 'lodash';
import { v4 } from 'uuid';
import { StripeChargeResponse, StripeTokenResponse } from './stripe.service';

export interface KushkiTokenResponse {
  token: string;
}

export interface KushkiBinInfoResponse {
  number?: {
    [k: string]: any;
  };
  scheme: string;
  type?: string;
  brand: string;
  prepaid?: boolean;
  country?: {
    numeric?: string;
    alpha2?: string;
    name?: string;
    currency?: string;
    latitude?: number;
    longitude?: number;
    [k: string]: any;
  };
  bank?: {
    name: string;
    url?: string;
    phone?: string;
    city?: string;
    [k: string]: any;
  };
}

export interface KushkiChargeResponse {
  ticketNumber: string;
  transactionReference: string;
  details?: {
    responseCode?: string;
    responseText?: string;
    recap?: string;
    approvedTransactionAmount?: number;
    transactionId: string;
    transactionReference?: string;
    approvalCode: string;
    cardHolderName: string;
    lastFourDigits: string;
    binCard: string;
    processorName?: string;
    processorBankName: string;
    merchantName: string;
    currencyCode?: string;
    taxes?: {
      [k: string]: any;
    };
    transactionType: string;
    numberOfMonths?: number;
    processorId?: string;
    merchantId?: string;
    paymentBrand: string;
    created?: number;
    requestAmount?: number;
    subtotalIva?: number;
    subtotalIva0?: number;
    ivaValue?: number;
    iceValue?: number;
    transactionStatus?: string;
    metadata?: {
      [k: string]: any;
    };
    subscriptionMetadata?: {
      [k: string]: any;
    };
    subscriptionId?: string;
    method?: string;
    syncMode?: 'api' | 'file' | 'online';
    saleTicketNumber?: string;
    graceMonths?: string;
    acquirerBank?: string;
    cardCountry?: string;
  };
}

@Injectable()
export class KushkiService {
  constructor(private readonly _httpService: HttpService) {}

  token(
    card: Card,
    gateway: Gateway,
    amount: number,
    currency: string,
  ): Observable<StripeTokenResponse> {
    return this._httpService
      .post<KushkiTokenResponse>(
        `${environment.kushkiBaseURL}tokens`,
        {
          card: {
            name: card.name,
            number: card.number,
            expiryMonth: card.expiryMonth,
            expiryYear: card.expiryYear,
            cvv: card.cvc,
          },
          totalAmount: amount,
          currency: currency,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Public-Merchant-Id': gateway.credential.publicCredential,
          },
        },
      )
      .pipe(
        mergeMap((response: AxiosResponse<KushkiTokenResponse>) =>
          forkJoin([
            of(response.data),
            this._httpService.get<KushkiBinInfoResponse>(
              `${environment.kushkiBaseURL}bin/${card.number.slice(0, 6)}`,
              {
                headers: {
                  'Content-Type': 'application/json',
                  'Public-Merchant-Id': gateway.credential.publicCredential,
                },
              },
            ),
          ]),
        ),
        map(
          ([token, binInfo]: [
            KushkiTokenResponse,
            AxiosResponse<KushkiBinInfoResponse>,
          ]) => {
            return <StripeTokenResponse>{
              id: token.token,
              created: new Date().getTime(),
              card: {
                brand: get(binInfo, 'data.brand'),
                country: get(binInfo, 'data.country'),
                funding: get(binInfo, 'data.cardType'),
                last4: card.number.slice(
                  card.number.length - 4,
                  card.number.length,
                ),
              },
            };
          },
        ),
        catchError((error: AxiosError) => {
          throw new HttpException(
            error.response.data.message,
            error.response.status,
          );
        }),
      );
  }

  charge(
    gateway: Gateway,
    amount: Amount,
    token: string,
    currency: string,
    description?: string,
    metadata?: Record<string, any>,
  ): Observable<StripeChargeResponse> {
    return this._httpService
      .post<KushkiChargeResponse>(
        `${environment.kushkiBaseURL}charges`,
        {
          token,
          amount: {
            currency,
            subtotalIva: amount.subtotalTax,
            subtotalIva0: amount.subtotal,
            iva: 0,
            ice: 0,
          },
          metadata: {
            ...metadata,
            description,
          },
          fullResponse: 'v2',
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Private-Merchant-Id': gateway.credential.privateCredential,
          },
        },
      )
      .pipe(
        map((response: AxiosResponse<KushkiChargeResponse>) => {
          set(response, 'data.details', { ...response.data });
          set(
            response,
            'data.amount',
            Number(amount.subtotal) + Number(amount.subtotalTax),
          );
          set(response, 'data.currency', currency);
          set(
            response,
            'data.responseCode',
            get(response, 'data.details.details.responseCode'),
          );
          set(
            response,
            'data.responseText',
            get(response, 'data.details.details.responseText'),
          );
          set(response, 'data.metadata', { ...metadata });
          set(response, 'data.status', 'succeeded');
          set(response, 'data.created', get(response, 'data.details.details.created'));
          set(response, 'data.description', description);
          set(response, 'data.id', get(response, 'data.transactionReference'));
          set(response, 'data.provider', 'kushki');

          return <StripeChargeResponse>(<unknown>response.data);
        }),
        catchError((error: AxiosError) => {
          if (!Boolean(get(error, 'response.data.transactionReference')))
            throw new HttpException(
              error.response.data.message,
              error.response.status,
            );

          const response = {
            currency,
            id: get(error, 'response.data.transactionReference'),
            created: new Date().getTime(),
            amount: Number(amount.subtotal) + Number(amount.subtotalTax),
            status: 'declined',
            provider: 'kushki'
          };

          set(response, 'details', error.response.data);
          set(response, 'responseCode', get(error, 'response.data.code'));
          set(response, 'responseText', get(error, 'response.data.message'));
          set(response, 'metadata', { ...metadata });
          set(response, 'description', description);

          return of(<StripeChargeResponse>(<unknown>response));
        }),
      );
  }
}
