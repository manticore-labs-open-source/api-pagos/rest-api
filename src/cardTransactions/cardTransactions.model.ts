import { Document, Schema } from 'mongoose';
import { IsNotEmpty, IsNumber, IsObject, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

const CardInfoSchema = new Schema({
  brand: String,
  type: String,
  cardId: String,
  bin: String,
  last4: String,
  country: String,
  name: String,
});

const CardTokenSchema = new Schema({
  card: CardInfoSchema,
  token: { type: String, required: true, index: true },
  created: { type: Number, required: true },
});

export const CardTransactionSchema = new Schema({
  created: Number,
  token: { type: CardTokenSchema, required: true },
  amount: Number,
  type: String,
  status: String,
  responseCode: String,
  responseText: String,
  description: String,
  currency: String,
  gatewayId: String,
  metadata: Object,
  details: Object,
  provider: String,
  transactionReference: { type: String, index: true },
});

export interface CardTransaction extends Document {
  id: string;
  created: number;
  token: Token;
  amount: number;
  type: string;
  status: string;
  responseCode: string;
  responseText: string;
  description: string;
  currency: string;
  gatewayId: string;
  metadata: Record<string, any>;
  details: Record<string, any>;
  transactionReference: string;
  provider: string;
}

export interface Token {
  card?: Card;
  token: string;
  created?: number;
}

export interface Card {
  number: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;
  name?: string;
}

export interface Amount {
  subtotalTax: number;
  subtotal: number;
}

class CardDto {
  @IsString()
  @IsNotEmpty()
  number: string;

  @IsString()
  @IsNotEmpty()
  expiryMonth: string;

  @IsString()
  @IsNotEmpty()
  expiryYear: string;

  @IsString()
  @IsNotEmpty()
  cvc: string;

  @IsString()
  @IsNotEmpty()
  name: string;
}

export class TokenDto {
  @IsString()
  @IsNotEmpty()
  currency: string;

  @IsString()
  @IsNotEmpty()
  gatewayId: string;

  @IsNumber()
  @IsNotEmpty()
  amount: number;

  @ValidateNested()
  @Type(() => CardDto)
  card: CardDto;
}

class AmountDto {
  @IsNumber()
  @IsNotEmpty()
  subtotalTax: number;

  @IsNumber()
  @IsNotEmpty()
  subtotal: number;
}

export class ChargeDto {
  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  @IsNotEmpty()
  token: string;

  @IsObject()
  @IsOptional()
  metadata: Record<string, any>;

  @IsString()
  @IsNotEmpty()
  currency: string;

  @ValidateNested()
  @Type(() => AmountDto)
  amount: AmountDto;
}
