import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import * as path from 'path'
import { environment } from '../environment/environment';

@Injectable()
export class ClientMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    if (/[^\\/]+\.[^\\/]+$/.test(req.path)) {
      const file = path.join(environment.spaPath, req.path).replace("/admin/", "/");

      res.sendFile(file, (err) => {
        if (err) {
          res.status(404).end();
        }
      });
    } else {
      return next();
    }
  }
}
