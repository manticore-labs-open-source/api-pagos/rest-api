import { Controller, Get, Render, Req, Res } from '@nestjs/common';
import { environment } from './environment/environment';
import { get } from 'lodash';

@Controller()
export class AppController {
  @Get()
  @Render('index')
  public index() {
    return {
      title: 'UniPayGW',
      env: {
        AUTH0_CLIENT_ID: environment.auth0ClientId,
        AUTH0_DOMAIN: environment.auth0Domain,
        AUTH0_CALLBACK_URL: environment.auth0CallbackUrl,
      },
    };
  }

  @Get('/login')
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public login() {}

  @Get('/callback')
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public callback() {}

  @Get('/api/user')
  public user(@Req() req) {
    return {
      picture: get(req, 'user.picture', ''),
      displayName: get(req, 'user.displayName', ''),
    };
  }

  @Get('/logout')
  public logout(@Req() req, @Res() res) {
    req.logout();
    res.redirect('/');
  }
}
